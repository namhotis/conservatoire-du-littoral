import logo from './logo.svg';
import Homepage from './pages/Homepage'
import './App.css';

function App() {
  return (
    <div className="App">
      <p>Bienvenue sur notre application</p>
      <Homepage />
    </div>
  );
}

export default App;
